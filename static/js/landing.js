(function(){

    /* Smooth scroll */
	smoothScroll.init({
	    selector: '[data-scroll]',
	    selectorHeader: '[data-scroll-header]', 
	    speed: 500, 
	    easing: 'easeInOutCubic', 
	    updateURL: false, 
	    offset: 0, 
	    callback: function(toggle, anchor){}
	});

	var d = document;
	var navOpen = false;

	var $ = function(id){
		return d.getElementById(id);
	}

	var nav = $("topbar");

	function addClass(e,c){
		if(e.className.indexOf(c)==-1)
			e.className += " "+c;
	}

	function removeClass(e,c){
		var re = new RegExp(c,"g");
		e.className = e.className.replace(re , "");
	}

	function openNav(){
		removeClass(nav, "closed");
		addClass(nav, "open");
		navOpen = true;
	}

	function closeNav(){
		removeClass(nav, "open");
		addClass(nav, "closed");
		navOpen = false;
	}

	function toggleNav(){
		if(navOpen){
			closeNav();
		} else {
			openNav();
		}
	}

	$("topbar").onclick = toggleNav;

})();

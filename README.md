# Installation Instructions

Just install the pip dependencies: `sudo pip3 install -r requirements.txt`

# Running the Server

Just execute `./wsgi.py production`. Use `./wsgi.py` if you're working on the
code, the server will automatically detect changes in this mode.

# Deploying to OpenShift

## OpenShift Setup

First setup your openshift account. Then setup your rhc client (if you didn't do
so before): `rhc setup`

## Create Application

Now create a new application (may take some time):

```
rhc app create <application_name> python-3.3 mysql-5.5 phpMyAdmin-4 --no-git
```

This creates a nonscalable application with a database and phpMyAdmin for
debugging purposes.

## Add OpenShift Remote

Retrieve the status, most importantly the Git URL:

```
rhc app-show <application_name>
```

Add the Git URL you saw as a remote to this repository:

```
git remote add os <OPENSHIFT_REPO_URL>
```

You should now be able to push directly to openshift with `git push os`.

## Setup secret keys as environment variables.

```
rhc env set <variable>=<value> <variable2>=<value2> -a <application_name>
```
import os, datetime
import uuid
from flask import Flask, flash, request, render_template, make_response
from validate_email import validate_email
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__, static_url_path='')
app.secret_key = os.getenv("SESSION_SECRET", "IamAdummySecretKey123987@#")

try:
    app.config['SQLALCHEMY_DATABASE_URI'] = \
        ('mysql+pymysql://{user}:{password}@{host}:{port}/{db}'
            .format(
                user=os.environ['OPENSHIFT_MYSQL_DB_USERNAME'],
                password=os.environ['OPENSHIFT_MYSQL_DB_PASSWORD'],
                host=os.environ['OPENSHIFT_MYSQL_DB_HOST'],
                port=os.environ['OPENSHIFT_MYSQL_DB_PORT'],
                db=os.environ['OPENSHIFT_APP_NAME']))
except:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'

db = SQLAlchemy(app)


class Visitor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime)
    track_id = db.Column(db.String(255))
    version = db.Column(db.String(255))

    def __init__(self, track_id, version):
        self.track_id = track_id
        self.version = version
        self.timestamp = datetime.datetime.utcnow()


class Registration(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime)
    track_id = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    version = db.Column(db.String(255))

    def __init__(self, track_id, version, email):
        self.track_id = track_id
        self.version = version
        self.email = email
        self.timestamp = datetime.datetime.utcnow()

db.create_all()

VERSIONS = ['index_management.html']
user_count = 0


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@app.route('/home', methods=['GET', 'POST'])
def index():
    try:
        version = request.cookies['_ver']
        assert version in VERSIONS
    except (KeyError, AssertionError):
        global user_count
        version = VERSIONS[user_count % len(VERSIONS)]
        user_count = user_count + 1

    track_id = request.cookies.get('_tid', str(uuid.uuid4()))
    db.session.add(Visitor(track_id, version))
    db.session.commit()

    message = {'title': 'GitMate'}

    if 'email' in request.form:
        email = request.form['email']

        if validate_email(email):
            user = Registration.query.filter_by(email=email).first()

            if user is None:
                db.session.add(Registration(track_id, version, email))
                db.session.commit()
                flash('Thank you for subscribing.')
            else:
                flash("You're a keen one, huh? You've already registered!")
        else:
            flash('Invalid E-Mail address - typed it right?')

    response = make_response(render_template(version, message=message))
    response.set_cookie('_ver', version)
    response.set_cookie('_tid', track_id)
    return response

#!/usr/bin/env python3
import sys
from landing import app as application

if __name__ == '__main__':
    if "production" not in sys.argv:
        application.debug=True
        application.run(port=8051)
    else:
        from wsgiref.simple_server import make_server
        httpd = make_server('localhost', 8051, application)
        httpd.serve_forever()
